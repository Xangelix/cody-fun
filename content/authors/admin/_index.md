---
# Display name
name: Cody Neiman

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Information Security

# Organizations/Affiliations
organizations:
- name: NASA - Kennedy Space Center
  url: "https://nasa.gov"

# Short bio (displayed in user profile at end of posts)
bio: Chasing inspirations...

interests:
- Technology
- Music
- Chemistry
- Physics

education:
  courses:
  - course: IB DP Diploma
    institution: Cocoa Beach Junior/Senior High School
    year: Actively Seeking
  - course: IB MYP Diploma
    institution: Cocoa Beach Junior/Senior High School
    year: 2018
  - course: IB PYP Diploma
    institution: Freedom 7 Elementary School of International Studies
    year: 2014

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:cody-neiman@cody.fun'  # For a direct email link, use "mailto:test@example.org".
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/cody-neiman/
- icon: github
  icon_pack: fab
  link: https://github.com/xangelix
- icon: project-diagram
  icon_pack: fas
  link: https://matrix.to/#/@cody-neiman:cody.fun
- icon: discord
  icon_pack: fab
  link: https://discord.gg/eyBGTRg
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.  
- icon: cv
  icon_pack: ai
  link: files/CodyNeimanResumeJan2019.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
user_groups:
- Admin
- Researcher
---

*Cody Neiman* is a student of *Cocoa Beach Junior/Senior High School* currently
working as an *Information Security Intern* at the *Kennedy Space Center (NASA)*
in *Cape Canaveral, Florida*. He is actively seeking an
*International Baccalaureate (IB) Diploma* while developing his knowledge on
*Information Systems* himself.
