+++
# Accomplishments widget.
widget = "accomplishments"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 50  # Order that this section will appear.

title = "Accomplish&shy;ments"
subtitle = ""

# Date format
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Accomplishments.
#   Add/remove as many `[[item]]` blocks below as you like.
#   `title`, `organization` and `date_start` are the required parameters.
#   Leave other parameters empty if not required.
#   Begin/end multi-line descriptions with 3 quotes `"""`.

[[item]]
  organization = "CompTIA"
  organization_url = "https://www.comptia.org"
  title = "CompTIA Secure Infrastructure Specialist – CSIS Stackable Certification"
  url = "https://certification.comptia.org/certifications/which-certification/stackable-certifications"
  certificate_url = "https://www.youracclaim.com/badges/7b5ccb17-eee7-4edc-851a-4a8cc14a0c75/"
  date_start = "2019-05-24"
  date_end = "2022-05-23"
  description = """<img src="img/csis.png" alt="CompTIA Secure Infrastructure Specialist – CSIS Stackable Certification"
	title="CompTIA Secure Infrastructure Specialist – CSIS Stackable Certification" width="150" />"""

[[item]]
  organization = "CompTIA"
  organization_url = "https://www.comptia.org"
  title = "CompTIA Security+ ce Certification"
  url = "https://certification.comptia.org/certifications/security"
  certificate_url = "https://www.youracclaim.com/badges/c74833aa-bef7-415b-8294-5105f60f7c93/"
  date_start = "2019-05-23"
  date_end = "2022-05-23"
  description = """<img src="img/security+.png" alt="CompTIA Security+ ce Certification"
	title="CompTIA Security+ ce Certification" width="150" />"""

[[item]]
  organization = "CompTIA"
  organization_url = "https://www.comptia.org"
  title = "CompTIA IT Operations Specialist – CIOS Stackable Certification"
  url = "https://certification.comptia.org/certifications/which-certification/stackable-certifications"
  certificate_url = "https://www.youracclaim.com/badges/5fbdfdb7-4c60-4389-a5c8-48077a90c5e9/"
  date_start = "2019-04-30"
  date_end = "2022-05-23"
  description = """<img src="img/cios.png" alt="CompTIA IT Operations Specialist – CIOS Stackable Certification"
	title="CompTIA IT Operations Specialist – CIOS Stackable Certification" width="150" />"""
  
[[item]]
  organization = "CompTIA"
  organization_url = "https://www.comptia.org"
  title = "CompTIA Network+ ce Certification"
  url = "https://certification.comptia.org/certifications/network"
  certificate_url = "https://www.youracclaim.com/badges/3fade39c-02df-4dad-ab10-e4e72c7a72c3/"
  date_start = "2019-04-29"
  date_end = "2022-05-23"
  description = """<img src="img/network+.png" alt="CompTIA Network+ ce Certification"
	title="CompTIA Network+ ce Certification" width="150" />"""

[[item]]
  organization = "Microsoft"
  organization_url = "https://microsoft.com"
  title = "MTA: Networking Fundamentals"
  url = "https://www.microsoft.com/en-us/learning/exam-98-366.aspx"
  certificate_url = "https://www.youracclaim.com/badges/67b9e588-82e7-4d74-a5f5-50ab767688f3/"
  date_start = "2018-08-27"
  date_end = ""
  description = """<img src="img/mta-networking-fundamentals.png" alt="MTA: Networking Fundamentals"
	title="MTA: Networking Fundamentals" width="150" />"""


[[item]]
  organization = "CompTIA"
  organization_url = "https://www.comptia.org"
  title = "CompTIA A+ ce Certification"
  url = "https://certification.comptia.org/certifications/a"
  certificate_url = "https://www.youracclaim.com/badges/06fcdfc6-e6cb-446c-a9db-f28c883eeddf/"
  date_start = "2018-04-03"
  date_end = "2022-05-23"
  description = """<img src="img/a+.png" alt="CompTIA A+ ce Certification"
	title="CompTIA A+ ce Certification" width="150" />"""


[[item]]
  organization = "Microsoft"
  organization_url = "https://microsoft.com"
  title = "Microsoft Office 2013 Specialist"
  url = "https://www.microsoft.com/en-us/learning/microsoft-office-specialist-certification-2013.aspx"
  certificate_url = "https://www.youracclaim.com/users/cody-neiman/badges"
  date_start = "2016-01-01"
  date_end = ""
  description = """<img src="img/mos.png" alt="Microsoft Office 2013 Specialist"
	title="Microsoft Office 2013 Specialist" width="150" />"""

+++
